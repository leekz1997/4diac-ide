/*******************************************************************************
 * Copyright (c) 2008 - 2017 Profactor GmbH, TU Wien ACIN, AIT, fortiss GmbH,
 * 				 2018, 2020 Johannes Kepler University Linz
 *
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Gerhard Ebenhofer, Alois Zoitl, Matthias Plasch, Filip Andren,
 *   Waldemar Eisenmenger, Martin Melik Merkumians
 *     - initial API and implementation and/or initial documentation
 *   Alois Zoitl - Refactored class hierarchy of xml exporters
 *   			 - New Project Explorer layout
 *******************************************************************************/
package org.eclipse.fordiac.ide.systemmanagement;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.fordiac.ide.model.dataexport.SystemExporter;
import org.eclipse.fordiac.ide.model.dataimport.SystemImporter;
import org.eclipse.fordiac.ide.model.libraryElement.AutomationSystem;
import org.eclipse.fordiac.ide.model.typelibrary.TypeLibrary;
import org.eclipse.fordiac.ide.systemmanagement.extension.ITagProvider;
import org.eclipse.fordiac.ide.systemmanagement.util.SystemPaletteManagement;
import org.eclipse.fordiac.ide.ui.editors.EditorUtils;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;

/**
 * The Class SystemManager.
 *
 * @author gebenh
 */
public enum SystemManager {

	INSTANCE;

	public static final String FORDIAC_PROJECT_NATURE_ID = "org.eclipse.fordiac.ide.systemmanagement.FordiacNature"; //$NON-NLS-1$
	public static final String OLD_DISTRIBUTED_PROJECT_NATURE_ID = "org.fordiac.systemManagement.DistributedNature"; //$NON-NLS-1$

	public static final String SYSTEM_FILE_ENDING = "sys"; //$NON-NLS-1$
	public static final String SYSTEM_FILE_ENDING_WITH_DOT = ".sys"; //$NON-NLS-1$

	public static final String TYPE_LIB_FOLDER_NAME = "Type Library"; //$NON-NLS-1$

	/** The model systems. */
	private Map<IProject, Map<IFile, AutomationSystem>> allSystemsInWS = new HashMap<>();

	private final Map<AutomationSystem, ArrayList<ITagProvider>> tagProviders = new HashMap<>();

	/** The listeners. */
	private List<DistributedSystemListener> listeners = new ArrayList<>();

	/**
	 * Notify listeners.
	 */
	public void notifyListeners() {
		for (DistributedSystemListener listener : listeners) {
			listener.distributedSystemWorkspaceChanged();
		}
	}

	/**
	 * Adds the workspace listener.
	 *
	 * @param listener the listener
	 */
	public void addWorkspaceListener(final DistributedSystemListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Instantiates a new system manager.
	 */
	private SystemManager() {
		// Correctly setup the tool library needs to be done before loading any systems
		// and adding the resource change listener
		TypeLibrary.loadToolLibrary();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(new FordiacResourceChangeListener(this));
	}

	public IProject createNew4diacProject(String projectName, IPath location, boolean importDefaultPalette,
			IProgressMonitor monitor) throws CoreException {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		IProject project = root.getProject(projectName);
		IProjectDescription description = ResourcesPlugin.getWorkspace().newProjectDescription(project.getName());

		if (!Platform.getLocation().equals(location)) {
			description.setLocation(location);
		}

		description.setNatureIds(getNatureIDs());

		project.create(description, monitor);
		project.open(monitor);

		// configure palette
		if (importDefaultPalette) {
			SystemPaletteManagement.copyToolTypeLibToDestination(project.getFolder(TYPE_LIB_FOLDER_NAME));
		}
		getProjectSystems(project); // insert the project into the project list
		return project;
	}

	public synchronized AutomationSystem createNewSystem(IContainer location, String name) {
		IFile systemFile = location.getFile(new Path(name + SystemManager.SYSTEM_FILE_ENDING_WITH_DOT));
		Map<IFile, AutomationSystem> projectSystems = getProjectSystems(location.getProject());
		AutomationSystem system = projectSystems.computeIfAbsent(systemFile, SystemImporter::createAutomationSystem);
		saveSystem(system);
		return system;
	}

	public synchronized void removeProject(IProject project) {
		allSystemsInWS.remove(project);
		notifyListeners();
	}

	/**
	 * Remove a system from the set of systems managed by the system manager
	 *
	 * @param system to be added
	 */
	public void removeSystem(final AutomationSystem system) {
		removeSystem(system.getSystemFile());
	}

	public synchronized void removeSystem(final IFile systemFile) {
		Map<IFile, AutomationSystem> projectSystems = getProjectSystems(systemFile.getProject());
		AutomationSystem refSystem = projectSystems.remove(systemFile);
		if (null != refSystem) {
			closeAllSystemEditors(refSystem);
			notifyListeners();
		}
	}

	/**
	 * Load system.
	 *
	 *
	 * systemFile xml file for the system
	 *
	 * @return the automation system
	 */
	private static AutomationSystem loadSystem(final IFile systemFile) {
		if (systemFile.exists()) {
			SystemImporter sysImporter = new SystemImporter(systemFile);
			sysImporter.loadElement();
			return sysImporter.getElement();
		}
		return null;
	}

	public void saveTagProvider(AutomationSystem system, ITagProvider tagProvider) {
		IProject project = system.getSystemFile().getProject();
		IPath projectPath = project.getLocation();
		tagProvider.saveTagConfiguration(projectPath);
	}

	public String getReplacedString(AutomationSystem system, String value) {
		ArrayList<ITagProvider> tagProvider = tagProviders.get(system);
		String result = null;
		for (ITagProvider iTagProvider : tagProvider) {
			result = iTagProvider.getReplacedString(value);
			if (result != null) {
				break;
			}
		}
		return result;
	}

	/**
	 * Save system.
	 *
	 * @param system the system
	 * @param all    the all
	 */
	public static void saveSystem(final AutomationSystem system) {
		SystemExporter systemExporter = new SystemExporter(system);
		systemExporter.saveSystem(system.getSystemFile());
	}

	public List<AutomationSystem> getSystems() {
		return Collections.emptyList();
	}

	public synchronized AutomationSystem getSystem(IFile systemFile) {
		Map<IFile, AutomationSystem> projectSystems = getProjectSystems(systemFile.getProject());
		return projectSystems.computeIfAbsent(systemFile, sysFile -> {
			long startTime = System.currentTimeMillis();
			AutomationSystem system = loadSystem(systemFile);
			long endTime = System.currentTimeMillis();
			System.out.println(
					"Loading time for System (" + systemFile.getName() + "): " + (endTime - startTime) + " ms"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return system;
		});
	}

	public synchronized Map<IFile, AutomationSystem> getProjectSystems(IProject project) {
		return allSystemsInWS.computeIfAbsent(project, p -> new HashMap<>());
	}

	public ITagProvider getTagProvider(Class<?> class1, AutomationSystem system) {
		if (!tagProviders.containsKey(system)) {
			tagProviders.put(system, new ArrayList<>());
		}
		ITagProvider provider = null;
		ArrayList<ITagProvider> tagProviderList = tagProviders.get(system);
		if (tagProviderList != null) {
			for (ITagProvider iTagProvider : tagProviderList) {
				if (iTagProvider.getClass().equals(class1)) {
					provider = iTagProvider;
					break;
				}
			}
		}
		if (provider == null) {
			try {
				Object obj = class1.getDeclaredConstructor().newInstance();
				if (obj instanceof ITagProvider) {
					provider = (ITagProvider) obj;
					provider.initialzeNewTagProvider();
					saveTagProvider(system, provider);
					tagProviderList.add(provider);
				}
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				return null;
			}
		}
		return provider;
	}

	private static String[] getNatureIDs() {
		return new String[] { SystemManager.FORDIAC_PROJECT_NATURE_ID };
	}

	private static void closeAllSystemEditors(final AutomationSystem refSystem) {
		// display related stuff needs to run in a display thread
		Display.getDefault().asyncExec(
				() -> EditorUtils.closeEditorsFiltered((IEditorPart editor) -> (editor instanceof ISystemEditor)
						&& (refSystem.equals(((ISystemEditor) editor).getSystem()))));

	}

}
